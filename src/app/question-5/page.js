export default function Question5() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q5</p>
                <p className="fn-q-content">같이 무인도에 떨어진 사람이 계속 울고 있다. 이 때 나는?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">무섭긴 하겠지만.. <br/>빨리 일을 시작해야 하는데.. 답답하다 ( 2점 )</button>
                <button className="fn-btn">나도 무서워... 옆에 앉아서 같이 운다 ( 5점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
