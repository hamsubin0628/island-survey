export default function Question9() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q9</p>
                <p className="fn-q-content">사람들 사이에서 갈등이 생겼다. 이 때 나는?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">누가 먼저 잘못했는지 판단한다 ( 1점 )</button>
                <button className="fn-btn">일단 화해하라며 악수를 시킨다 ( 5점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
