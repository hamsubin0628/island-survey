export default function Question4() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q4</p>
                <p className="fn-q-content">내가 무인도에 떨어지고 제일 먼저 할 생각은?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">물은 어디서 찾지? 여긴 어딜까? ( 4점 )</button>
                <button className="fn-btn">살아서 나갈 수 있을까? ( 2점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
