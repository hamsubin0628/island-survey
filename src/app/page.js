export default function Home() {
  return (
    <div className="fn-app">
      <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
        <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
        <div className="fn-q-section">
            <p className="fn-q">Q1</p>
            <p className="fn-q-content">나는 무인도에 떨어지면 어떡하지? 같은 생각을 해본 적이</p>
        </div>
        <form className="fn-btn-section">
            <button className="fn-btn">종종 있다 ( 5점 )</button>
            <button className="fn-btn">드물거나 거의 없다 ( 2점 )</button>
        </form>
        <p className="fn-sources">출처: 테스트봄</p>
    </div>
  );
}
