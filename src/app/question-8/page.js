export default function Question8() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q8</p>
                <p className="fn-q-content">무인도에서 내가 맡을 역할은?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">자원을 수집하기 ( 3점 )</button>
                <button className="fn-btn">탈출 계획을 세우기 ( 1점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
