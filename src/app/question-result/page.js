export default function QuestionResult() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q-content">결과입니다</p>
                <p className="fn-result">생존 전문가 🏹</p>
            </div>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
