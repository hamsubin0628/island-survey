export default function Question6() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q6</p>
                <p className="fn-q-content">내가 혼자 무인도에 떨어졌다면?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">필요한 자원이 생기면 그때그때 구한다 ( 4점 )</button>
                <button className="fn-btn">힘들어도 일단 최대한 많은 자원을 구해 놓는다 ( 1점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
