export default function Question10() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q10</p>
                <p className="fn-q-content">일단 무인도의 환경을 파악하려고 한다. 이 때 나는?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">무인도를 직접 돌아다녀본다 ( 1점 )</button>
                <button className="fn-btn">숲 중간까지만 들어가 보고<br/> 지형이겠거니 예상해본다 ( 3점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
