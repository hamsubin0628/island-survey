export default function Question2() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q2</p>
                <p className="fn-q-content">내가 무인도 생존에서 가장 두려운 것은?</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">낯선 환경에 떨어졌다는 것 ( 2점 )</button>
                <button className="fn-btn">아무도 없이 혼자 있다는 것 ( 3점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
