export default function Question3() {
    return (
        <div className="fn-app">
            <h1 className="fn-title">🏝 무인도 생존 테스트 🏝</h1>
            <h5 className="fn-sub-title">아무도 없는 무인도! 내가 무인도에 떨어진다면?</h5>
            <div className="fn-q-section">
                <p className="fn-q">Q3</p>
                <p className="fn-q-content">당신은 무인도에 떨어진다면</p>
            </div>
            <form className="fn-btn-section">
                <button className="fn-btn">혼자라서 너무 외로울 것 같다 ( 5점 )</button>
                <button className="fn-btn">무섭지만 혼자라 편할 것 같기도 하다 ( 1점 )</button>
            </form>
            <p className="fn-sources">출처: 테스트봄</p>
        </div>
    );
}
